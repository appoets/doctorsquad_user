package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.IRegisterPresenter;

public interface IRegisterView extends IView<IRegisterPresenter> {
    void goToLogin();
    void goToHome();
}
