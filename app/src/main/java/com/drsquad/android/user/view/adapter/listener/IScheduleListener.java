package com.drsquad.android.user.view.adapter.listener;

import com.drsquad.android.user.model.dto.response.ScheduleResponse;

public interface IScheduleListener extends BaseRecyclerListener<ScheduleResponse> {
}
