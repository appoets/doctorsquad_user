package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.IEditProfilePresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IEditProfileView extends IView<IEditProfilePresenter> {
}
