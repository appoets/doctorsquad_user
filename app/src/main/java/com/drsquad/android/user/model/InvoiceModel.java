package com.drsquad.android.user.model;

import com.drsquad.android.user.model.dto.request.InvoiceRequest;
import com.drsquad.android.user.model.dto.response.InvoiceResponse;
import com.drsquad.android.user.model.listener.IModelListener;
import com.drsquad.android.user.model.webservice.ApiClient;
import com.drsquad.android.user.model.webservice.ApiInterface;

import java.util.List;

public class InvoiceModel extends BaseModel<InvoiceResponse> {

    public InvoiceModel(IModelListener<InvoiceResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(InvoiceResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<InvoiceResponse> response) {

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void requestInvoice(InvoiceRequest request){
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).requestInvoice(request));
    }
}
