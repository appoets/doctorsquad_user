package com.drsquad.android.user.view.iview;

import android.os.Bundle;

import com.drsquad.android.user.model.dto.response.invoice.Invoice;
import com.drsquad.android.user.presenter.ipresenter.IInvoicePresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IInvoiceView extends IView<IInvoicePresenter> {
        void setUp(Bundle bundle);
        void updateInvoiceData(Invoice data);
        void paymentSuccess();
}
