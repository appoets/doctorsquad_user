package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.model.dto.response.ScheduleResponse;
import com.drsquad.android.user.presenter.ipresenter.ISchedulePresenter;
import com.drsquad.android.user.view.adapter.listener.IScheduleListener;

import java.util.List;

public interface IScheduleView extends IView<ISchedulePresenter> {
    void setAdapter(List<ScheduleResponse> list, IScheduleListener iScheduleListener);
    void moveToDetail(ScheduleResponse data);
    void initSetUp();
}
