package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.INotificationPresenter;
import com.drsquad.android.user.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
}
