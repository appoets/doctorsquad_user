package com.drsquad.android.user.presenter.ipresenter;

import com.drsquad.android.user.model.dto.request.RatingRequest;

/**
 * Created by Tranxit Technologies.
 */

public interface IRatingPresenter extends IPresenter  {
    void postRating(RatingRequest request);
}
