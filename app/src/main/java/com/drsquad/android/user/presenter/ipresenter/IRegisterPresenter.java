package com.drsquad.android.user.presenter.ipresenter;

import com.drsquad.android.user.model.dto.request.LoginRequest;
import com.drsquad.android.user.model.dto.request.RegisterRequest;

public interface IRegisterPresenter extends IPresenter {
    void goToLogin();
    void postLogin(LoginRequest request);
    void postRegister(RegisterRequest registerRequest);
}