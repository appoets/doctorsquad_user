package com.drsquad.android.user.view.iview;


import com.drsquad.android.user.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
