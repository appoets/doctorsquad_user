package com.drsquad.android.user.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drsquad.android.user.R;
import com.drsquad.android.user.model.dto.common.History;
import com.drsquad.android.user.view.adapter.listener.IHistoryRecyclerAdapter;
import com.drsquad.android.user.view.adapter.viewholder.HistoryViewHolder;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryRecyclerAdater extends BaseRecyclerAdapter<IHistoryRecyclerAdapter,History,HistoryViewHolder> {

    List<History> historyList;
    IHistoryRecyclerAdapter iHistoryRecyclerAdapter;

    public HistoryRecyclerAdater(List<History> historyList, IHistoryRecyclerAdapter iHistoryRecyclerAdapter) {
        super(historyList, iHistoryRecyclerAdapter);
        this.historyList = historyList;
        this.iHistoryRecyclerAdapter = iHistoryRecyclerAdapter;

    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new HistoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.history_list_item,parent,false),iHistoryRecyclerAdapter);
    }
}
