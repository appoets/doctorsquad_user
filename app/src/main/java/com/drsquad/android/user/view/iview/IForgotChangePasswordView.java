package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.IForgotChangePasswordPresenter;

public interface IForgotChangePasswordView extends IView<IForgotChangePasswordPresenter> {
                void goToLogin();
}
