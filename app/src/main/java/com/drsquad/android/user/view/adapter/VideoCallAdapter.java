package com.drsquad.android.user.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drsquad.android.user.R;
import com.drsquad.android.user.model.dto.response.VideoCallResponse;
import com.drsquad.android.user.view.adapter.listener.IVideoCallListener;
import com.drsquad.android.user.view.adapter.viewholder.VideocallViewHolder;

import java.util.List;

public class VideoCallAdapter extends BaseRecyclerAdapter<IVideoCallListener, VideoCallResponse, VideocallViewHolder> {

    private List<VideoCallResponse> videocallList;
    private IVideoCallListener iVideoCallListener;

    public VideoCallAdapter(List<VideoCallResponse> videocallList, IVideoCallListener iVideoCallListener) {
        super(videocallList, iVideoCallListener);
        this.videocallList = videocallList;
        this.iVideoCallListener = iVideoCallListener;

    }

    @NonNull
    @Override
    public VideocallViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VideocallViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.videocall_list_item, parent, false), iVideoCallListener);
    }
}