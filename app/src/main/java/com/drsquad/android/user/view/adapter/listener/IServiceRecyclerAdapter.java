package com.drsquad.android.user.view.adapter.listener;

import com.drsquad.android.user.model.dto.common.Services;

/**
 * Created by Tranxit Technologies.
 */

public interface IServiceRecyclerAdapter extends BaseRecyclerListener<Services> {
}
