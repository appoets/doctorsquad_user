package com.drsquad.android.user.model.dto.response;

import com.google.gson.annotations.SerializedName;
import com.drsquad.android.user.model.dto.common.History;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class HistoryResponse extends BaseResponse {

    @SerializedName("history")
    private List<History> historyList;

    public List<History> getHistoryList() {
        return historyList;
    }

    public void setHistoryList(List<History> historyList) {
        this.historyList = historyList;
    }
}
