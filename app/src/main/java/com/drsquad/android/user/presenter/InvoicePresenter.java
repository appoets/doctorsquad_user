package com.drsquad.android.user.presenter;

import android.os.Bundle;

import com.drsquad.android.user.model.CustomException;
import com.drsquad.android.user.model.InvoiceModel;
import com.drsquad.android.user.model.PaymentModel;
import com.drsquad.android.user.model.dto.request.InvoiceRequest;
import com.drsquad.android.user.model.dto.request.PaymentRequest;
import com.drsquad.android.user.model.dto.response.BaseResponse;
import com.drsquad.android.user.model.dto.response.InvoiceResponse;
import com.drsquad.android.user.model.listener.IModelListener;
import com.drsquad.android.user.presenter.ipresenter.IInvoicePresenter;
import com.drsquad.android.user.view.iview.IInvoiceView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class InvoicePresenter extends BasePresenter<IInvoiceView> implements IInvoicePresenter {

    public InvoicePresenter(IInvoiceView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp(bundle);
    }

    @Override
    public void requestInvoice(InvoiceRequest request) {
        iView.showProgressbar();
        new InvoiceModel(new IModelListener<InvoiceResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull InvoiceResponse response) {
                iView.dismissProgressbar();
                iView.updateInvoiceData(response.getInvoice());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<InvoiceResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).requestInvoice(request);
    }

    @Override
    public void requestPayment(PaymentRequest request) {
        iView.showProgressbar();
        new PaymentModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
//                iView.paymentSuccess();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).requestPayment(request);
    }
}
