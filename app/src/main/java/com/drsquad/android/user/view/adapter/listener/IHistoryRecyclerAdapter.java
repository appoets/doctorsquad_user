package com.drsquad.android.user.view.adapter.listener;

import com.drsquad.android.user.model.dto.common.History;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryRecyclerAdapter extends BaseRecyclerListener<History> {
}
