package com.drsquad.android.user.presenter.ipresenter;

public interface  IOnBoardPresenter extends IPresenter {

    void goToLogin();
}
