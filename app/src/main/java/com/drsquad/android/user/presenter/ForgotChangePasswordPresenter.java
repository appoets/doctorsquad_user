package com.drsquad.android.user.presenter;

import com.drsquad.android.user.presenter.ipresenter.IForgotChangePasswordPresenter;
import com.drsquad.android.user.view.iview.IForgotChangePasswordView;


public class ForgotChangePasswordPresenter extends BasePresenter<IForgotChangePasswordView> implements IForgotChangePasswordPresenter {

    public ForgotChangePasswordPresenter(IForgotChangePasswordView iView) {
        super(iView);
    }

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }
}
