package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.model.dto.common.History;
import com.drsquad.android.user.presenter.ipresenter.IHistoryPresenter;
import com.drsquad.android.user.view.adapter.HistoryRecyclerAdater;

/**
 * Created by Tranxit Technologies.
 */

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryRecyclerAdater adapter);
    void moveToChat(History data);
    void initSetUp();
}
