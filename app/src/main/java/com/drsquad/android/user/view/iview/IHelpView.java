package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.model.dto.response.HelpResponse;
import com.drsquad.android.user.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
