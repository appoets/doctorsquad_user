package com.drsquad.android.user.presenter.ipresenter;

public interface IVideoCallPresenter extends IPresenter {
    void getVideoCallList();
}
