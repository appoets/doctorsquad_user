package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.ILoginPresenter;

public interface ILoginView extends IView<ILoginPresenter> {
    void goToRegistration();
    void goToForgotPassword();
    void goToHome();
}
