package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.IChatPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IChatView extends IView<IChatPresenter> {
        void setUp();
}
