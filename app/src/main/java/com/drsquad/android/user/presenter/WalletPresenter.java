package com.drsquad.android.user.presenter;

import android.os.Bundle;

import com.drsquad.android.user.model.CustomException;
import com.drsquad.android.user.model.AddDeleteCardModel;
import com.drsquad.android.user.model.GetCardModel;
import com.drsquad.android.user.model.dto.response.BaseResponse;
import com.drsquad.android.user.model.dto.response.CardResponse;
import com.drsquad.android.user.model.listener.IModelListener;
import com.drsquad.android.user.presenter.ipresenter.IWalletPresenter;
import com.drsquad.android.user.view.iview.IWalletView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class WalletPresenter extends BasePresenter<IWalletView> implements IWalletPresenter {

    public WalletPresenter(IWalletView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }

    @Override
    public void getCard() {
        iView.showProgressbar();
        new GetCardModel(new IModelListener<CardResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull CardResponse response) {
                iView.dismissProgressbar();
                iView.onSuccess(response.getCards());
            }

            @Override
            public void onSuccessfulApi(@NotNull List<CardResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).getCardDetail();
    }

    @Override
    public void addCard(String token) {
        iView.showProgressbar();
        new AddDeleteCardModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.onAddCard();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).addCard(token);
    }

    @Override
    public void deleteCard(String cardId) {
        iView.showProgressbar();
        new AddDeleteCardModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.onDeleteCard();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).deleteCard(cardId);
    }
}
