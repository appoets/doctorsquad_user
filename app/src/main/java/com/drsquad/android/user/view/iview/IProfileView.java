package com.drsquad.android.user.view.iview;



import com.drsquad.android.user.model.dto.response.ProfileResponse;
import com.drsquad.android.user.presenter.ipresenter.IProfilePresenter;

public interface IProfileView extends IView<IProfilePresenter> {
    void updateUserDetails(ProfileResponse response);
}
