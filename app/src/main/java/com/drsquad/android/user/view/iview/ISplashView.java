package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.ISplashPresenter;

public interface ISplashView extends IView<ISplashPresenter> {

    void startTimer(int splashTimer);

    void gotoLogin();

    void gotoHome();

}
