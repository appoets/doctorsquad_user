package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.IMakeCallPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IMakeCallView extends IView<IMakeCallPresenter> {
        void setUp();
}
