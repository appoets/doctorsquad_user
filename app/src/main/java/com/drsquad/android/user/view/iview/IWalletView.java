package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.model.dto.response.Cards;
import com.drsquad.android.user.presenter.ipresenter.IWalletPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IWalletView extends IView<IWalletPresenter> {
    void setUp();
    void onSuccess(Cards cards);
    void onAddCard();
    void onDeleteCard();
}
