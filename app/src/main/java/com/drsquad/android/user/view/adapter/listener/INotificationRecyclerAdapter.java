package com.drsquad.android.user.view.adapter.listener;

import com.drsquad.android.user.model.dto.response.NotificationResponse;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<NotificationResponse> {
}
