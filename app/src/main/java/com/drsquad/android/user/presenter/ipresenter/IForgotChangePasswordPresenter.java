package com.drsquad.android.user.presenter.ipresenter;

public interface IForgotChangePasswordPresenter extends IPresenter {
    void goToLogin();
}