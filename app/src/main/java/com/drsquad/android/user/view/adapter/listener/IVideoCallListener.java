package com.drsquad.android.user.view.adapter.listener;

import com.drsquad.android.user.model.dto.response.VideoCallResponse;

public interface IVideoCallListener extends BaseRecyclerListener<VideoCallResponse> {
}
