package com.drsquad.android.user.view.iview;

import com.drsquad.android.user.presenter.ipresenter.ITwilloVideoPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface ITwilloVideoView extends IView<ITwilloVideoPresenter> {
    void twiloVideoToken(Object token);
}
