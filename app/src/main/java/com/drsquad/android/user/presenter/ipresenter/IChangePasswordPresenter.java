package com.drsquad.android.user.presenter.ipresenter;

import com.drsquad.android.user.model.dto.request.ChangePasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void changePassword(ChangePasswordRequest request);
}